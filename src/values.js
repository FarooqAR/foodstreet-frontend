import { VERIFY_ERROR } from './redux/actions/types';
export const months = [
  'January',
  'February',
  'March',
  'April',
  'May',
  'June',
  'July',
  'August',
  'September',
  'October',
  'November',
  'December',
];
const INVALID_EMAIL_PASS = 'Email or Password is incorrect';
const NETWORK_ERROR = 'There was a problem with the internet';
const UNKNOWN_ERROR = 'An unknown error occurred';
export const errorMessages = {
  api: {
    [VERIFY_ERROR]: 'The given credentials could not be verified',
  },
  firebase: {
    'auth/internal-error': UNKNOWN_ERROR,
    'auth/user-not-found': INVALID_EMAIL_PASS,
    'auth/invalid-email': INVALID_EMAIL_PASS,
    'auth/wrong-password': INVALID_EMAIL_PASS,
    'auth/network-request-failed': NETWORK_ERROR,
    'auth/user-disabled': 'This account has been disabled. Contact us for further details',
  },
};
export const rootUrl = 'http://localhost:3001/api';
