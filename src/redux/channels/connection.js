import firebase from 'firebase/app';
import { eventChannel, END } from 'redux-saga';

let ref; // we need to retain ref so we can later unsubscribe

export default function connection(user) {
  return eventChannel((emit) => {
    if (user) {
      ref = firebase.database()
        .ref('.info/connected');

      ref.on('value', (snapshot) => {
        emit({
          value: snapshot.val(),
        });
      });
    } else {
      emit(END);
    }
    // return the unsubscribe function
    return () => {
      ref.off('value');
    };
  });
}
