import { LOGGING, LOGIN_ERROR, CLEAR_LOGIN_MESSAGE, VERIFIED, VERIFY_ERROR, LOGOUT } from '../actions/types';

const INITIAL_LOGIN_STATUS = {
  type: '',
  message: '',
};

export function currentUserReducer(state = null, action) {
  switch (action.type) {
    case VERIFIED:
      return action.currentUser;
    case VERIFY_ERROR:
      return null;
    default:
      return state;
  }
}
export function loginStatusReducer(state = INITIAL_LOGIN_STATUS, action) {
  switch (action.type) {
    case LOGGING:
      return { type: 'loading', message: action.message };
    case LOGIN_ERROR:
    case VERIFY_ERROR:
      return { type: 'error', message: action.message };
    case CLEAR_LOGIN_MESSAGE:
      return INITIAL_LOGIN_STATUS;
    default:
      return state;
  }
}

export function verifyStatusReducer(state = false, action) {
  switch (action.type) {
    case VERIFIED:
      return true;
    case LOGOUT:
      return false;
    default:
      return state;
  }
}
