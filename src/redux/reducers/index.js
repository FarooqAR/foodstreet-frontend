import { combineReducers } from 'redux';
import { persistReducer } from 'redux-persist'
import storage from 'redux-persist/lib/storage'
import modal from './modal';
import { currentUserReducer, loginStatusReducer, verifyStatusReducer } from './auth';
import statusReducer from './status';
import offlineStatusReducer from './connection';

const rootReducer = combineReducers({
  modal,
  currentUser: currentUserReducer,
  loginStatus: loginStatusReducer,
  verifyStatus: verifyStatusReducer,
  status: statusReducer,
  offline: offlineStatusReducer,
});

export default persistReducer(
  {
    key: 'foodstreet',
    storage,
    blacklist: ['verifyStatus'],
  },
  rootReducer,
);
