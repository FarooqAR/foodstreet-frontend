import { OFFLINE_STATUS } from '../actions/types';

export default function offlineStatusReducer(state = false, action) {
  switch (action.type) {
    case OFFLINE_STATUS:
      return action.value;
    default:
      return state;
  }
}
