import { FETCH_STATUS } from '../actions/types'; 

const INITIAL_STATE = {
  delivery: false,
  dinein: false,
  takeaway: false,
};

export default function (state = INITIAL_STATE, action) {
  switch (action.type) {
    case FETCH_STATUS:
      return { ...state, ...action.status };
    default:
      return state;
  }
}
