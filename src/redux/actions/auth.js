import { LOGIN, LOGOUT, CLEAR_LOGIN_MESSAGE, VERIFY_TOKEN } from './types';

export function login(email, password) {
  return {
    type: LOGIN,
    email,
    password,
  };
}
export function verifyToken(currentUser) {
  return {
    type: VERIFY_TOKEN,
    currentUser,
  };
}
export function logout() {
  return {
    type: LOGOUT,
  };
}
export function clearLoginStatus() {
  return {
    type: CLEAR_LOGIN_MESSAGE,
  };
}
