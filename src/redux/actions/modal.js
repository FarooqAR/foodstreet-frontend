import { SHOW_MODAL, HIDE_MODAL } from './types';

export function showModal(modalType, modalProps) {
  return {
    type: SHOW_MODAL,
    modalType,
    modalProps,
  };
}


// This will hide all the modals regardless of their types
export function hideModal() {
  return {
    type: HIDE_MODAL,
  };
}
