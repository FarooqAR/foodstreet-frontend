export const LOGIN = 'LOGIN';
export const LOGGING = 'LOGGING';
export const LOGOUT = 'LOGOUT';
export const LOGIN_ERROR = 'LOGIN_ERROR';
export const CLEAR_LOGIN_MESSAGE = 'CLEAR_LOGIN_MESSAGE';
export const VERIFY_TOKEN = 'VERIFY_TOKEN';
export const VERIFY_ERROR = 'VERIFY_ERROR';
export const VERIFIED = 'VERIFIED';

export const CHANGE_STATUS = 'CHANGE_STATUS';
export const FETCH_STATUS = 'FETCH_STATUS';
export const OFFLINE_STATUS = 'OFFLINE_STATUS';

export const SHOW_MODAL = 'SHOW_MODAL';
export const HIDE_MODAL = 'HIDE_MODAL';

export const MAP_MODAL = 'MAP_MODAL';
export const USER_DETAILS_MODAL = 'USER_DETAILS_MODAL';
export const ENTRY_DETAILS_MODAL = 'ENTRY_DETAILS_MODAL';
export const ADD_SHOP_ITEM_MODAL = 'ADD_SHOP_ITEM_MODAL';
export const PAYMENTS_MODAL = 'PAYMENTS_MODAL';
