import { CHANGE_STATUS } from './types';

export function changeStatus(statusType: 'all' | 'delivery' | 'dinein' | 'takeaway', value: boolean) {
  return {
    type: CHANGE_STATUS,
    statusType,
    value,
  };
}
