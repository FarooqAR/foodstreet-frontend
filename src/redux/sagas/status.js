import { take, call, fork, put, select } from 'redux-saga/effects';
import { FETCH_STATUS, CHANGE_STATUS, VERIFIED, LOGOUT } from '../actions/types';
import { status } from '../channels/status';
import { changeStatus } from '../../services/status';

function* watchChangeStatus() {
  while (true) {
    const { statusType, value } = yield take(CHANGE_STATUS);
    const currentUser = yield select(state => state.currentUser);
    console.log(statusType, value, currentUser);
    try {
      yield call(changeStatus, statusType, value, currentUser.uid);
    } catch (e) {
      console.log(e);
    }
  }
}

function* watchFetchStatus(currentUser) {
  const statusChannel = yield call(status, currentUser);
  while (true) {
    try {
      const statusObj = yield take(statusChannel);
      yield put({ type: FETCH_STATUS, status: statusObj.value });
    } catch (e) {
      console.log(e);
      statusChannel.close();
    }
  }
}

function* watchFetchStatusListener() {
  while (true) {
    const { currentUser } = yield take([VERIFIED, LOGOUT]);
    yield fork(watchFetchStatus, currentUser);
  }
}

export default function* root() {
  yield fork(watchFetchStatusListener);
  yield fork(watchChangeStatus);
}
