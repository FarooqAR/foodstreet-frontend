import { take, call, fork, put } from 'redux-saga/effects';
import { signInWithEmailAndPassword, signOut, getToken, verifyToken } from '../../services/auth';
import { LOGIN, LOGGING, LOGOUT, LOGIN_ERROR, VERIFY_TOKEN, VERIFY_ERROR, VERIFIED, CLEAR_LOGIN_MESSAGE } from '../actions/types';
import { errorMessages } from '../../values';

function* login() {
  while (true) {
    const { email, password } = yield take(LOGIN);
    yield put({ type: LOGGING, message: 'Signing In' });
    try {
      yield call(signInWithEmailAndPassword, email, password);
    } catch (e) {
      console.log(e);
      yield put({ type: LOGIN_ERROR, message: errorMessages.firebase[e.code] });
    }
  }
}
function* verify() {
  while (true) {
    const { currentUser } = yield take(VERIFY_TOKEN);
    yield put({ type: LOGGING, message: 'Verifying' });
    try {
      const token = yield call(getToken);
      yield call(verifyToken, token);
      yield put({ type: VERIFIED, currentUser });
      yield put({ type: CLEAR_LOGIN_MESSAGE });
    } catch (e) {
      console.log(e);
      yield put({ type: VERIFY_ERROR, message: errorMessages.api[VERIFY_ERROR] });
    }
  }
}
function* logout() {
  while (true) {
    yield take(LOGOUT);
    try {
      yield call(signOut);
    } catch (e) {
      console.log(e);
    }
  }
}

export default function* root() {
  yield fork(login);
  yield fork(logout);
  yield fork(verify);
}
