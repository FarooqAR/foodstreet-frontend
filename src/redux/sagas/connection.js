import { take, call, fork, put } from 'redux-saga/effects';
import { OFFLINE_STATUS, VERIFIED, LOGOUT } from '../actions/types';
import connection from '../channels/connection';

function* watchConnection(currentUser) {
  const connectionChannel = yield call(connection, currentUser);
  while (true) {
    try {
      const { value } = yield take(connectionChannel);
      yield put({ type: OFFLINE_STATUS, value: !value });
    } catch (e) {
      console.log(e);
      connectionChannel.close();
    }
  }
}

function* watchConnectionListener() {
  while (true) {
    const { currentUser } = yield take([VERIFIED, LOGOUT]);
    yield fork(watchConnection, currentUser);
  }
}

export default function* root() {
  yield fork(watchConnectionListener);
}
