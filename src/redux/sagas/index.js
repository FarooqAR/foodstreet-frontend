import { fork, all } from 'redux-saga/effects';
import auth from './auth';
import status from './status';
import connection from './connection';

export default function* root() {
  yield all([
    fork(auth),
    fork(status),
    fork(connection),
  ]);
}
