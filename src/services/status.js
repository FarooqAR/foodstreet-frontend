import firebase from 'firebase/app';

function getStatusRef(uid) {
  return firebase.database()
    .ref('restaurants')
    .child(uid)
    .child('status');
}
export function changeStatus(statusType, value, uid) {
  if (statusType === 'all') {
    return getStatusRef(uid).set({
      delivery: value,
      dinein: value,
      takeaway: value,
    });
  }
  return getStatusRef(uid)
    .child(statusType)
    .set(value);
}
