import firebase from 'firebase/app';
import { rootUrl } from '../values';

export function signInWithEmailAndPassword(email, password) {
  return firebase.auth().signInWithEmailAndPassword(email, password);
}

export function getToken() {
  return firebase.auth().currentUser.getIdToken();
}

export function signOut() {
  return firebase.auth().signOut();
}

export function verifyToken(token) {
  return fetch(`${rootUrl}/auth/verify`, {
    method: 'POST',
    headers: new Headers({
      Authorization: token,
      'Content-Type': 'application/x-www-form-urlencoded',
    }),
  })
  .then(r => r.json())
  .then(e => {
    if (e.error)
      throw new Error(e.error);
  });
}
