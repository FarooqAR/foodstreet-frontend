/* eslint jsx-a11y/no-noninteractive-element-to-interactive-role: "off" */
import React from 'react';
import PropTypes from 'prop-types';
import '../styles/switch.css';

const Switch = props => (
  <label className="switch" htmlFor={props.name} role="switch" aria-checked={props.isOn ? 'true' : 'false'}>
    <input type="checkbox" id={props.name} onChange={props.handleToggle} className="switch__toggle" checked={props.isOn} />
    <span className="switch__label" />
  </label>
);

Switch.propTypes = {
  name: PropTypes.string.isRequired,
  isOn: PropTypes.bool.isRequired,
  handleToggle: PropTypes.func.isRequired,
};

export default Switch;
