import React from 'react';
import PropTypes from 'prop-types';
import ProgressiveImage from 'react-progressive-image';

const AsyncImage = props => (
  <ProgressiveImage src={props.src}>
    {(src, loading) => (
      loading ?
        <span
          className="img-placeholder"
          style={{ width: props.width, height: props.height }}
        /> :
        <img
          src={src}
          style={{ width: props.width, height: props.height }}
          alt={props.alt}
          className={props.className}
        />
    )}
  </ProgressiveImage>
);

AsyncImage.propTypes = {
  width: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
  height: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
  src: PropTypes.string.isRequired,
  className: PropTypes.string,
  alt: PropTypes.string,
};

AsyncImage.defaultProps = {
  alt: 'default-alt',
  className: '',
  width: 'unset',
  height: 'unset',
};

export default AsyncImage;
