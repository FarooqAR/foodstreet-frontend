import React, { Component } from 'react';
import PropTypes from 'prop-types';
import TimeAgo from 'react-timeago';
import { Popover, PopoverHeader, PopoverBody } from 'reactstrap';
import { formattedTime } from '../utils/time';
import '../styles/card.css';

class DineInCard extends Component {
  state = {}
  componentWillMount() {
    const state = {};
    this.props.preOrder.forEach((item) => {
      state[`popoverOpen-${this.props.id}-${item.id}`] = false;
    });
    this.setState(state);
  }
  toggle = (cardId, itemId) => {
    const key = `popoverOpen-${cardId}-${itemId}`;
    this.setState(prevState => ({
      [key]: !prevState[key],
    }));
  }
  render() {
    const {
      id,
      userBy,
      time,
      timeIn,
      heads,
      preOrder,
      booked,
    } = this.props;

    const formattedTimeIn = formattedTime(timeIn);

    return (
      <div className={`card dinein ${booked ? 'inprogress' : 'new'}`}>
        <div className="card-header">
          <div className="card-header-info">
            <div className="left">
              <div className="card-title">{userBy.name}</div>
              <div className="card-header-info-item time-in">
                <span className="icon alarm-icon" />{' '}
                <span className="info-item-label">
                  {formattedTimeIn}
                </span>
              </div>
            </div>
            <div className="right">
              <div className="card-header-info-item time-in">
                <div className="info-item-count">
                  {heads}
                </div>
                <div className="info-item-label">Persons</div>
              </div>
            </div>
          </div>
        </div>
        <div className="card-body">
          {preOrder.length > 0 ? (
            <div className="includes">
              <span className="includes-label">Pre Order:</span>
              <div className="includes-list">
                {preOrder.map(inc => (
                  <span tabIndex={inc.id} role="button" onClick={() => this.toggle(id, inc.id)} key={inc.id} id={`item-${id}-${inc.id}`} className={`includes-item ${inc.extras && inc.extras.length > 0 ? 'with-extras' : ''}`}>
                    <span className="includes-item-label">{inc.title}</span>
                    <span className="includes-item-count">{inc.quantity}</span>
                    <Popover className="includes-item-details-popover" placement="top" isOpen={this.state[`popoverOpen-${id}-${inc.id}`]} target={`item-${id}-${inc.id}`} toggle={() => this.toggle(id, inc.id)}>
                      <PopoverHeader>
                        <span className="popover-title">{inc.title}</span>{' '}
                        <span className="price">Rs. {inc.price}</span>
                      </PopoverHeader>
                      <PopoverBody>
                        {inc.extras && inc.extras.length > 0 ? inc.extras.map(extra => (
                          <div className="popover-extra-item" key={extra.id}>
                            <span className="popver-extra-item-label">{extra.label}</span>
                            <span className="popver-extra-item-price">Rs. {extra.price}</span>
                          </div>
                        )) : 'No Extras'}
                      </PopoverBody>
                    </Popover>
                  </span>
                ))}
              </div>
            </div>
          ) : <div className="includes" /> }
          <div className="phone-address">
            <div className="phone"><span className="icon phone-icon" /> +92 000 0000000</div>
            <div className="time">
              <span className="icon clock-icon" />{' '}
              <TimeAgo date={time} />
            </div>
          </div>
        </div>
        <div className="card-footer">
          {booked ? null :
          <div className="left">
            <button className="custom-btn">Accept</button>
          </div>}
          <div className="right">
            <button className="custom-btn">Decline</button>
          </div>
        </div>
      </div>

    );
  }
}

DineInCard.propTypes = {
  id: PropTypes.number.isRequired,
  userBy: PropTypes.object.isRequired,
  time: PropTypes.number.isRequired,
  timeIn: PropTypes.number.isRequired,
  heads: PropTypes.number.isRequired,
  preOrder: PropTypes.array,
  booked: PropTypes.bool,
};

DineInCard.defaultProps = {
  preOrder: [],
  booked: false,
};

export default DineInCard;
