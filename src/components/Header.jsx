import React from 'react';
import {
  Collapse,
  Navbar,
  NavbarToggler,
  NavbarBrand,
  Nav,
  NavItem,
  NavLink,
  UncontrolledDropdown,
  DropdownToggle,
  DropdownMenu,
  DropdownItem } from 'reactstrap';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import Switch from '../components/Switch';
import { changeStatus } from '../redux/actions/status';
import '../styles/header.css';

type Props = {
  onLogout: () => void,
  changeStatus: (statusType: string, value: boolean) => void,
}

class Header extends React.Component<Props> {
  static getDerivedStateFromProps(nextProps) {
    return {
      isRestaurantClosed: (
        nextProps.deliveryStatus ||
        nextProps.dineinStatus ||
        nextProps.takeawayStatus
      ),
      isDeliveryClosed: nextProps.deliveryStatus,
      isDineInClosed: nextProps.dineinStatus,
      isTakeawayClosed: nextProps.takeawayStatus,
    };
  }
  constructor(props) {
    super(props);
    this.state = {
      isOpen: false,
      isRestaurantClosed: false,
      isDeliveryClosed: false,
      isTakeawayClosed: false,
      isDineInClosed: false,
    };
  }
  handleNavbarToggle = () => {
    this.setState({
      isOpen: !this.state.isOpen,
    });
  }
  handleRestaurantStatusToggle = () => {
    this.setState(prevState => ({
      isRestaurantClosed: !prevState.isRestaurantClosed,
      isDeliveryClosed: !prevState.isRestaurantClosed,
      isTakeawayClosed: !prevState.isRestaurantClosed,
      isDineInClosed: !prevState.isRestaurantClosed,
    }), () => {
      this.props.changeStatus('all', this.state.isRestaurantClosed);
    });
  }
  handleDeliveryStatusToggle = () => {
    console.log('delivery');
    this.setState(prevState => ({
      isRestaurantClosed: (
        !prevState.isDeliveryClosed &&
        prevState.isTakeawayClosed &&
        prevState.isDineInClosed
      ),
      isDeliveryClosed: !prevState.isDeliveryClosed,
    }), () => {
      this.props.changeStatus('delivery', this.state.isDeliveryClosed)
    });
  }
  handleTakeawayStatusToggle = () => {
    this.setState(prevState => ({
      isRestaurantClosed: (
        prevState.isDeliveryClosed &&
        !prevState.isTakeawayClosed &&
        prevState.isDineInClosed
      ),
      isTakeawayClosed: !prevState.isTakeawayClosed,
    }), () => {
      this.props.changeStatus('takeaway', this.state.isTakeawayClosed);
    });
  }
  handleDineInStatusToggle = () => {
    this.setState(prevState => ({
      isRestaurantClosed: (
        prevState.isDeliveryClosed &&
        prevState.isTakeawayClosed &&
        !prevState.isDineInClosed
      ),
      isDineInClosed: !prevState.isDineInClosed,
    }), () => {
      this.props.changeStatus('dinein', this.state.isDineInClosed);
    });
  }
  render() {
    const {
      isRestaurantClosed,
      isDeliveryClosed,
      isTakeawayClosed,
      isDineInClosed,
    } = this.state;
    return (
      <div className="header">
        <Navbar color="dark" dark expand="md">
          <NavbarBrand tag={Link} className="brand-logo" to="/delivery/new">FoodStreet</NavbarBrand>
          <NavbarToggler onClick={this.handleNavbarToggle} />
          <Collapse isOpen={this.state.isOpen} navbar>
            <Nav className="ml-auto" navbar>
              <NavItem>
                <NavLink tag={Link} to="/history">History</NavLink>
              </NavItem>
              <NavItem>
                <NavLink tag={Link} to="/myshop">Customize</NavLink>
              </NavItem>
              <NavItem>
                <NavLink tag={Link} to="/payments">Payments</NavLink>
              </NavItem>
              <UncontrolledDropdown className="hide-mobile" nav inNavbar>
                <DropdownToggle nav>
                  Status
                </DropdownToggle>
                <DropdownMenu className="status-dropdown-menu" flip right>
                  <DropdownItem toggle={false}>
                    <span>All</span>
                    <Switch
                      name="RestaurantStatus"
                      isOn={isRestaurantClosed}
                      handleToggle={this.handleRestaurantStatusToggle}
                    />
                  </DropdownItem>
                  <DropdownItem divider />
                  <DropdownItem toggle={false}>
                    <span>Delivery/Gift</span>
                    <Switch
                      name="DeliveryStatus"
                      isOn={isDeliveryClosed}
                      handleToggle={this.handleDeliveryStatusToggle}
                    />
                  </DropdownItem>
                  <DropdownItem toggle={false}>
                    <span>Dine In</span>
                    <Switch
                      name="DineInStatus"
                      isOn={isDineInClosed}
                      handleToggle={this.handleDineInStatusToggle}
                    />
                  </DropdownItem>
                  <DropdownItem toggle={false}>
                    <span>Takeaway</span>
                    <Switch
                      name="TakeawayStatus"
                      isOn={isTakeawayClosed}
                      handleToggle={this.handleTakeawayStatusToggle}
                    />
                  </DropdownItem>
                </DropdownMenu>
              </UncontrolledDropdown>
              <NavItem className="visible-mobile">
                <NavLink tag={Link} to="/settings">Settings</NavLink>
              </NavItem>
              <NavItem onClick={this.props.onLogout} className="visible-mobile">
                Logout
              </NavItem>
              <UncontrolledDropdown className="hide-mobile" nav inNavbar>
                <DropdownToggle nav>
                  <span className="icon icon-settings" />
                </DropdownToggle>
                <DropdownMenu right>
                  <DropdownItem className="dropdown-item-link">
                    <Link href="/settings" to="/settings">Settings</Link>
                  </DropdownItem>
                  <DropdownItem divider />
                  <DropdownItem onClick={this.props.onLogout}>
                    Logout
                  </DropdownItem>
                </DropdownMenu>
              </UncontrolledDropdown>
            </Nav>
          </Collapse>
        </Navbar>
      </div>
    );
  }
}

function mStateToProps({ status }) {
  return {
    deliveryStatus: status.delivery,
    dineinStatus: status.dinein,
    takeawayStatus: status.takeaway,
  };
}

function mDispatchToProps(dispatch) {
  return {
    changeStatus: (statusType: string, value: boolean) => dispatch(changeStatus(statusType, value)),
  };
}

export default connect(mStateToProps, mDispatchToProps)(Header);
