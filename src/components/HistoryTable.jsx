import React from 'react';
import PropTypes from 'prop-types';
import { Table } from 'reactstrap';
import deliveryIcon from '../images/motorcycle-grey.png';
import dineInIcon from '../images/dine-in-grey.png';
import takeawayIcon from '../images/takeaway-grey.png';
import cashIcon from '../images/coins.png';
import walletIcon from '../images/wallet.png';
import '../styles/table.css';

const renderType = (type) => {
  switch (type) {
    case 'delivery':
      return <img src={deliveryIcon} alt="type" title={type} />;
    case 'dinein':
      return <img src={dineInIcon} alt="type" title={type} />;
    case 'takeaway':
      return <img src={takeawayIcon} alt="type" title={type} />;
    case 'cash':
      return <img src={cashIcon} alt="type" title={type} />;
    case 'wallet':
      return <img src={walletIcon} alt="type" title={type} />;
    default:
      return null;
  }
};

const renderRows = (data, onEntryIdClicked, onNameClicked) => (
  data.map(entry => (
    <tr key={entry.id}>
      <td onClick={() => onEntryIdClicked(entry.id)} className="entry-cell-link">{entry.id}</td>
      <td>{renderType(entry.type)}</td>
      <td onClick={() => onNameClicked(entry.username)} className="entry-cell-link">{entry.name}</td>
      <td>{entry.payment}</td>
      <td>{renderType(entry.paymethod)}</td>
    </tr>
  ))
);

const HistoryTable = ({ data, onEntryIdClicked, onNameClicked }) => (
  <Table className="history-table" responsive>
    <thead>
      <tr>
        <th>#</th>
        <th>Type</th>
        <th>Name</th>
        <th>Payment</th>
        <th>Payment Method</th>
      </tr>
    </thead>
    <tbody>
      {renderRows(data, onEntryIdClicked, onNameClicked)}
    </tbody>
  </Table>
);

HistoryTable.propTypes = {
  onEntryIdClicked: PropTypes.func.isRequired,
  onNameClicked: PropTypes.func.isRequired,
  data: PropTypes.array.isRequired,
};

export default HistoryTable;
