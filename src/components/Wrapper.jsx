import React, { Component } from 'react';
import LoaderScreen from './LoaderScreen';

export default function Wrapper(importComponent) {
  class wrapper extends Component {
    constructor(props) {
      super(props);

      this.state = {
        component: null,
      };
    }

    async componentWillMount() {
      const { default: component } = await importComponent();

      this.setState({
        component,
      });
    }

    render() {
      const C = this.state.component;

      return C ? <C {...this.props} /> : <LoaderScreen />;
    }
  }

  return wrapper;
}
