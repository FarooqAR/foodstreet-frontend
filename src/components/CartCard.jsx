import React, { Component } from 'react';
import { Popover, PopoverHeader, PopoverBody } from 'reactstrap';
import TimeAgo from 'react-timeago';
import '../styles/card.css';

type IncludeItem = {
  id: number,
  title: string,
  price: number,
  quantity: number,
  extras: Array<{
    id: number,
    label: string,
    price: number
  }>
}

type Props = {
  id: number,
  userBy: {
    name: string,
  },
  time: number,
  cost: number,
  status?: string,
  includes: Array<IncludeItem>,
  userTo?: {
    name: string
  } | null,
  buttonsLeft?: Array<{
    label: string
  }>,
  buttonsRight?: Array<{
    label: string
  }>
}

class CartCard extends Component<Props> {
  static defaultProps = {
    userTo: null,
    status: '',
    buttonsLeft: [],
    buttonsRight: [],
  }
  state = {}
  componentWillMount() {
    const state = {};
    this.props.includes.forEach((item) => {
      state[`popoverOpen-${this.props.id}-${item.id}`] = false;
    });
    this.setState(state);
  }
  toggle = (cardId, itemId) => {
    const key = `popoverOpen-${cardId}-${itemId}`;
    this.setState(prevState => ({
      [key]: !prevState[key],
    }));
  }
  renderButtons = buttons => (
    buttons.map(btn => (
      <button
        key={btn.label}
        onClick={(() => (btn.onClick ? btn.onClick(this.props.id) : null))}
        className={btn.className || 'custom-btn'}
      >
        {btn.label}
      </button>
    ))
  )
  render() {
    const {
      id,
      userBy,
      userTo,
      time,
      cost,
      includes,
      status,
      buttonsRight,
      buttonsLeft,
    } = this.props;

    return (
      <div className={`card cart ${status}`}>
        <div className="card-header">
          <div className="card-title">{userBy.name}</div>
          <div className="card-header-info">
            {userTo === null ? null :
            <div className="card-header-info-item username-friend">
              <span className="icon gift-icon" />{' '}
              <span className="info-item-label user-label">{userTo.name}</span>
            </div>}
            <div className="card-header-info-item time">
              <span className="icon clock-icon" />{' '}
              <span className="info-item-label time-label">
                <TimeAgo date={time} />
              </span>
            </div>
            <div className="card-header-info-item cost">
              <span className="icon cost-icon" />{' '}
              <span className="info-item-label cost-label">{cost} Rs</span>
            </div>
          </div>
        </div>
        <div className="card-body">
          {status === 'done' ? <div className="includes" /> :
          <div className="includes">
            <span className="includes-label">Cart:</span>
            <div className="includes-list">
              {includes.map(inc => (
                <span tabIndex={inc.id} role="button" onClick={() => this.toggle(id, inc.id)} key={inc.id} id={`item-${id}-${inc.id}`} className={`includes-item ${inc.extras && inc.extras.length > 0 ? 'with-extras' : ''}`}>
                  <span className="includes-item-label">{inc.title}</span>
                  <span className="includes-item-count">{inc.quantity}</span>
                  <Popover className="includes-item-details-popover" placement="top" isOpen={this.state[`popoverOpen-${id}-${inc.id}`]} target={`item-${id}-${inc.id}`} toggle={() => this.toggle(id, inc.id)}>
                    <PopoverHeader>
                      <span className="popover-title">{inc.title}</span>{' '}
                      <span className="price">Rs. {inc.price}</span>
                    </PopoverHeader>
                    <PopoverBody>
                      {inc.extras && inc.extras.length > 0 ? inc.extras.map(extra => (
                        <div className="popover-extra-item" key={extra.id}>
                          <span className="popver-extra-item-label">{extra.label}</span>
                          <span className="popver-extra-item-price">Rs. {extra.price}</span>
                        </div>
                      )) : 'No Extras'}
                    </PopoverBody>
                  </Popover>
                </span>
              ))}
            </div>
          </div>}
          {status !== 'done' ? (
            <div className="phone-address">
              <div className="phone"><span className="icon phone-icon" /> +92 000 0000000</div>
              <div className="address"><span className="icon marker-icon" /> Block 2, Gulistan e Johar</div>
            </div>
          ) : null}
        </div>
        {status !== 'done' ? (
          <div className="card-footer">
            <div className="left">
              {this.renderButtons(buttonsLeft)}
            </div>
            <div className="right">
              {this.renderButtons(buttonsRight)}
            </div>
          </div>
        ) : null}
      </div>
    );
  }
}


// CartCard.propTypes = {
//   id: PropTypes.number.isRequired,
//   userBy: PropTypes.object.isRequired,
//   time: PropTypes.number.isRequired,
//   cost: PropTypes.number.isRequired,
//   includes: PropTypes.arrayOf(PropTypes.object).isRequired,
//   buttonsLeft: PropTypes.arrayOf(PropTypes.object),
//   buttonsRight: PropTypes.arrayOf(PropTypes.object),
//   userTo: PropTypes.object,
//   status: PropTypes.string,
// };

CartCard.defaultProps = {
  userTo: null,
  status: '',
  buttonsLeft: [],
  buttonsRight: [],
};

export default CartCard;
