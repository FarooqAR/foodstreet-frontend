import React from 'react';
import CircularProgress from '@material-ui/core/CircularProgress';
import '../styles/loaderscreen.css';

const LoaderScreen = () => (
  <div className="loader-screen">
    <div className="loader-inner">
      <h2>FoodStreet</h2>
      <CircularProgress size={20} style={{ color: '#fff' }} />
    </div>
  </div>
);

export default LoaderScreen;
