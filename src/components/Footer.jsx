import React from 'react';
import companyLogo from '../images/company-logo.png';
import '../styles/footer.css';

const Footer = () => (
  <div className="footer">
    <div className="left">
      <span className="brand-logo">FoodStreet</span>{' '}
      <span className="product-version">v0.0.0</span>
    </div>
    <div className="middle">
      <span className="company-copyrights">&copy; 2018</span>
    </div>
    <div className="right">
      <a href="https://beaver-62b49.firebaseapp.com/" rel="noopener noreferrer" target="_blank">
        <img className="company-logo" src={companyLogo} alt="beaver-logo" />
      </a>
    </div>
  </div>
);

export default Footer;
