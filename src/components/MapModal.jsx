import React from 'react';
import PropTypes from 'prop-types';
import { withScriptjs, withGoogleMap, GoogleMap, Marker } from 'react-google-maps';
import { Modal, ModalHeader, ModalBody, ModalFooter } from 'reactstrap';
import { connect } from 'react-redux';
import { hideModal } from '../redux/actions/modal';

const MapComponent = withScriptjs(withGoogleMap(() => (
  <GoogleMap
    defaultZoom={16}
    defaultCenter={{ lat: 24.9059329, lng: 67.1395157 }}
  >
    <Marker position={{ lat: 24.9050973, lng: 67.1380292 }} />
  </GoogleMap>
)));

const MapModal = (props) => {
  const { deliveryId, hideMap } = props;
  return (
    <Modal
      fade={false}
      isOpen
      toggle={hideMap}
    >
      <ModalHeader>Track {deliveryId}</ModalHeader>
      <ModalBody>
        <MapComponent
          googleMapURL="https://maps.googleapis.com/maps/api/js?key=AIzaSyD5-dM4rAO0Na9JiL2kfogsQX2dFGRymGs&v=3.exp&libraries=geometry,drawing,places"
          loadingElement={<div style={{ height: '100%' }} />}
          containerElement={<div style={{ height: 400 }} />}
          mapElement={<div style={{ height: '100%' }} />}
        />
      </ModalBody>
      <ModalFooter>
        <button className="custom-btn" onClick={hideMap}>Cancel</button>
      </ModalFooter>
    </Modal>
  );
};

MapModal.propTypes = {
  hideMap: PropTypes.func.isRequired,
  deliveryId: PropTypes.oneOfType([PropTypes.string, PropTypes.number]).isRequired,
};

export default connect(null, dispatch => ({
  hideMap: () => dispatch(hideModal()),
}))(MapModal);
