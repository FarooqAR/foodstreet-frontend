import React from 'react';
import PropTypes from 'prop-types';
import { Table } from 'reactstrap';
import duration from '../utils/duration';
import '../styles/table.css';

const renderRows = (data, onEntryIdClicked, onPayClicked) => (
  data.map(entry => (
    <tr key={entry.id}>
      <td onClick={() => onEntryIdClicked(entry.id)} className="entry-cell-link">{entry.id}</td>
      <td>{entry.from}</td>
      <td>{entry.to}</td>
      <td>{duration(864000000)}</td>
      <td>{entry.amount}</td>
      <td>{entry.cutOff}</td>
      <td>{entry.amountToBePaid}</td>
      <td>{entry.dueDate}</td>
      <td className={`${entry.status}${entry.duePast ? '-late' : ''}`}>{entry.status}</td>
      <td>{entry.paidOn || 'n/a'}</td>
      <td onClick={entry.status === 'paid' ? (() => {}) : () => onPayClicked(entry.id)} className="entry-cell-link">{entry.status === 'paid' ? null : 'Pay'}</td>
    </tr>
  ))
);

const PaymentsTable = ({ data, onPaymentIdClicked, onPayClicked }) => (
  <Table className="payments-table" responsive>
    <thead>
      <tr>
        <th>#</th>
        <th>From</th>
        <th>To</th>
        <th>Duration</th>
        <th>Amount</th>
        <th>CutOff (15%)</th>
        <th>Amount To Be Paid</th>
        <th>Due Date</th>
        <th>Status</th>
        <th>Paid On</th>
        <th>Actions</th>
      </tr>
    </thead>
    <tbody>
      {renderRows(data, onPaymentIdClicked, onPayClicked)}
    </tbody>
  </Table>
);

PaymentsTable.propTypes = {
  onPaymentIdClicked: PropTypes.func.isRequired,
  onPayClicked: PropTypes.func.isRequired,
  data: PropTypes.array.isRequired,
};

export default PaymentsTable;
