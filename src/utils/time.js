export function formattedTime(timestamp) {
  if (typeof timestamp !== 'number') {
    return null;
  }

  const days = {
    [-1]: 'Yesterday',
    0: 'Today',
    1: 'Tomorrow',
  };

  const originalDate = new Date(timestamp);
  const diffDay = originalDate.getDate() - (new Date()).getDate();
  let formattedDate = '';
  let h = originalDate.getHours();
  const m = originalDate.getMinutes();
  const ampm = h >= 12 ? 'PM' : 'AM';
  h = h > 12 ? h - 12 : h;
  const time = `${h < 10 ? 0 : ''}${h}:${m < 10 ? 0 : ''}${m} ${ampm}`;

  if (diffDay >= -1 && diffDay <= 1) {
    formattedDate += `${days[diffDay]}, ${time}`;
  } else {
    formattedDate += `${originalDate.toDateString()}, ${time}`;
  }

  return formattedDate;
}
