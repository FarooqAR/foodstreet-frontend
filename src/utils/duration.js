export default function (milliseconds) {
  if (milliseconds < 0) {
    return 'Invalid time';
  }
  const days = milliseconds / 86400000;
  if (days < 6.5) {
    return `${Math.round(days)} Day${Math.round(days) > 1 ? 's' : ''}`;
  } else if (Math.round(days / 7) < 4) {
    return `${Math.round(days / 7)} Week${Math.round(days / 7) >= 2 ? 's' : ''}`;
  } else if (Math.round(days / 7 / 4) < 12) {
    return `${Math.round(days / 7 / 4)} Month${Math.round(days / 7 / 4) >= 2 ? 's' : ''}`;
  }
  return `${Math.round(days / 7 / 4 / 12)} Year${Math.round(days / 7 / 4 / 12) >= 2 ? 's' : ''}`;
}
