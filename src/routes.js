import React from 'react';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import Wrapper from './components/Wrapper';
import App from './containers/App';

const NotFound = Wrapper(() => import('./components/404.jsx'));

const RouteComponent = () => (
  <Router>
    <Switch>
      <Route exact path="/404" component={NotFound} />
      <Route path="/" component={App} />
    </Switch>
  </Router>
);


export default RouteComponent;
