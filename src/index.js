import 'bootstrap/dist/css/bootstrap.css';
import 'whatwg-fetch';
import React from 'react';
import ReactDOM from 'react-dom';
import firebase from 'firebase/app';
import 'firebase/auth';
import 'firebase/database';
import { createStore, applyMiddleware, compose } from 'redux';
import { persistStore } from 'redux-persist';
import { Provider } from 'react-redux';
import createSagaMiddleware from 'redux-saga';
import rootReducer from './redux/reducers';
import RouteComponent from './routes';
import Loader from './components/LoaderScreen';
import registerServiceWorker from './registerServiceWorker';
import rootSaga from './redux/sagas';
import './styles/index.css';
import { PersistGate } from 'redux-persist/integration/react';

const sagaMiddleware = createSagaMiddleware();

/* eslint-disable no-underscore-dangle */
const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;
const store = createStore(
  rootReducer,
  composeEnhancers(applyMiddleware(sagaMiddleware)),
);
const persistor = persistStore(store);
/* eslint-enable */


const config = {
  apiKey: "AIzaSyCK1lwSrTt8TkXxNsqk7L7u7UXOUs0Davo",
  authDomain: "project-5900912580874279161.firebaseapp.com",
  databaseURL: "https://project-5900912580874279161.firebaseio.com",
  projectId: "project-5900912580874279161",
  storageBucket: "",
  messagingSenderId: "931860578833"
};
firebase.initializeApp(config);

ReactDOM.render(
  <Provider store={store}>
    <PersistGate loading={<Loader />} persistor={persistor}>
      <RouteComponent />
    </PersistGate>
  </Provider>,
  document.getElementById('root'),
  () => {
    sagaMiddleware.run(rootSaga);
  }
);


registerServiceWorker();
