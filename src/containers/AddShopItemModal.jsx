import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Modal, ModalHeader, ModalBody, ModalFooter, Input } from 'reactstrap';
import { connect } from 'react-redux';
import { hideModal } from '../redux/actions/modal';
import '../styles/addshopitemmodal.css';

class AddShopItemModal extends Component {
  state = {}
  render() {
    const { hide } = this.props;
    return (
      <Modal
        isOpen
        fade={false}
        toggle={hide}
      >
        <ModalHeader>Add Item</ModalHeader>
        <ModalBody>
          <Input type="text" placeholder="Title" />
          <Input type="text" placeholder="Subtitle" />
          <label htmlFor="input-item-image">
            <span>Select Item Image</span>
            <Input type="file" id="input-item-image" placeholder="Image" />
          </label>
          <Input type="select">
            <option disabled>Select size</option>
            <option value="full">Full</option>
            <option value="half">Half</option>
          </Input>
          <Input type="number" placeholder="Price" />
          <Input type="number" placeholder="Tax" />
        </ModalBody>
        <ModalFooter>
          <button className="custom-btn">Add</button>
          <button className="custom-btn" onClick={hide}>Cancel</button>
        </ModalFooter>
      </Modal>
    );
  }
}

AddShopItemModal.propTypes = {
  hide: PropTypes.func.isRequired,
};

export default connect(null, dispatch => ({
  hide: () => dispatch(hideModal()),
}))(AddShopItemModal);
