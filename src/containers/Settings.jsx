import React, { Component } from 'react';
import { Input } from 'reactstrap';
import '../styles/settings.css';

class Settings extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }
  render() {
    return (
      <div className="settings">
        <div className="change-pass">
          <h4>Change Password</h4>
          <hr />
          <form action="#">
            <Input type="password" placeholder="Old Password" />
            <Input type="password" placeholder="New Password" />
            <Input type="password" placeholder="Confirm Password" />
            <button type="submit" className="custom-btn primary">Update</button>
          </form>
          <h4>Change Organization Name</h4>
          <hr />
          <form action="#">
            <Input type="text" placeholder="Organization Name" />
            <button type="submit" className="custom-btn primary">Update</button>
          </form>
        </div>
      </div>
    );
  }
}

export default Settings;
