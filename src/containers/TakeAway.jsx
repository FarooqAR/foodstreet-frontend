import React, { Component } from 'react';
import { Switch, Route, NavLink, withRouter } from 'react-router-dom';
import PropTypes from 'prop-types';
import CartCard from '../components/CartCard';
import '../styles/cards.css';

const cards = [
  {
    id: 3000,
    userBy: { name: 'Farooq' },
    time: 1526538088388,
    cost: 400,
    includes: [
      {
        id: 1,
        title: 'Cheese Burger',
        price: 180,
        quantity: 1,
        extras: [{ id: 31, label: 'Extra a', price: 10 }, { id: 32, label: 'Extra b', price: 10 }],
      },
      {
        id: 2,
        title: 'Item 2',
        price: 40,
        quantity: 1,
      },
      {
        id: 3,
        title: 'Item 3',
        price: 40,
        quantity: 1,
      },
      {
        id: 4,
        title: 'Item 4',
        price: 40,
        quantity: 1,
      },
      {
        id: 5,
        title: 'Item 5',
        price: 40,
        quantity: 1,
      },
      {
        id: 6,
        title: 'Item 6',
        price: 40,
        quantity: 1,
      },
    ],
  },
  {
    id: 3001,
    userBy: { name: 'Muhammad' },
    time: 1526538088388 - (1000 * 60),
    cost: 350,
    includes: [
      {
        id: 1,
        title: 'Item 1',
        price: 250,
        quantity: 1,
      },
      {
        id: 2,
        title: 'Cheese Burger',
        price: 80,
        extras: [{ id: 31, label: 'Extra a', price: 10 }, { id: 32, label: 'Extra b', price: 10 }],
        quantity: 1,
      },
    ],
  },
  {
    id: 3002,
    userBy: { name: 'Mustafa' },
    time: 1526538088388 - (1000 * 60 * 60),
    cost: 100,
    includes: [
      {
        id: 1,
        title: 'Item 1',
        price: 50,
        quantity: 1,
      },
      {
        id: 2,
        title: 'Item 2',
        price: 50,
        quantity: 1,
      },
    ],
  },
  {
    id: 3003,
    userBy: { name: 'Shakoor' },
    time: 1526538088388 - (1000 * 60 * 100),
    cost: 130,
    includes: [
      {
        id: 1,
        title: 'Item 1',
        price: 130,
        quantity: 1,
      },
    ],
  },
];

class TakeAway extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }
  componentDidMount() {
    if (this.props.history.location.pathname === '/takeaway') {
      this.props.history.replace('/takeaway/new');
    }
  }
  renderCards = (c, status, buttonsLeft, buttonsRight) => (
    c.map(card => (
      <CartCard
        id={card.id}
        key={card.id}
        title={card.title}
        time={card.time}
        cost={card.cost}
        includes={card.includes || null}
        userBy={card.userBy}
        status={status}
        buttonsLeft={buttonsLeft}
        buttonsRight={buttonsRight}
      />
    ))
  )

  render() {
    return (
      <div className="takeaway">
        <div className="controls">
          <NavLink to="/takeaway/new" className="custom-link" activeClassName="active">New</NavLink>
          <NavLink to="/takeaway/inprogress" className="custom-link">In Progress</NavLink>
          <NavLink to="/takeaway/inwaiting" className="custom-link">In Waiting</NavLink>
        </div>
        <Switch>
          <Route
            exact
            path="/takeaway/new"
            component={() => (
              <div className="cards">
                {this.renderCards(cards, 'new', [
                  { label: 'Accept' },
                ], [
                  { label: 'Decline' },
                ])}
              </div>)}
          />
          <Route
            exact
            path="/takeaway/inprogress"
            component={() => (
              <div className="cards">
                {this.renderCards(cards.slice(1), 'inprogress', [], [
                  { label: 'Ready' },
                ])}
              </div>)}
          />
          <Route
            exact
            path="/takeaway/inwaiting"
            component={() => (
              <div className="cards">
                {this.renderCards(cards.slice(2), 'inwaiting', [], [
                  { label: 'Received' },
                ])}
              </div>)}
          />
        </Switch>
      </div>
    );
  }
}

TakeAway.propTypes = {
  history: PropTypes.object.isRequired,
};

export default withRouter(TakeAway);
