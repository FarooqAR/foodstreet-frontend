import React, { Component } from 'react';
import { Button, Input, Pagination, PaginationItem, PaginationLink } from 'reactstrap';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { showModal } from '../redux/actions/modal';
import { ENTRY_DETAILS_MODAL, USER_DETAILS_MODAL } from '../redux/actions/types';
import searchIcon from '../images/search16x16.png';
import HistoryTable from '../components/HistoryTable';
import '../styles/history.css';

class History extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }
  showEntryModal = (entryId) => {
    this.props.showModal(ENTRY_DETAILS_MODAL, {
      entryId,
    });
  }
  showUserDetailsModal = (username) => {
    this.props.showModal(USER_DETAILS_MODAL, {
      username,
    });
  }
  render() {
    const data = [
      {
        id: 2342,
        username: 'abc1',
        name: 'David Archer',
        payment: 200,
        type: 'delivery',
        paymethod: 'cash',
      },
      {
        id: 6534,
        username: 'abc1',
        name: 'Arnold Shalwar',
        payment: 400,
        type: 'delivery',
        paymethod: 'cash',
      },
      {
        id: 6336,
        username: 'abc1',
        name: 'The Rock',
        payment: 50,
        type: 'dinein',
        paymethod: 'wallet',
      },
      {
        id: 9888,
        name: 'Bill Games',
        payment: 200,
        type: 'delivery',
        paymethod: 'cash',
      },
      {
        id: 1001,
        username: 'abc1',
        name: 'Mark Zinger',
        payment: 190,
        type: 'takeaway',
        paymethod: 'cash',
      },
      {
        id: 3791,
        username: 'abc1',
        name: 'Habab Sheeren',
        payment: 100,
        type: 'takeaway',
        paymethod: 'wallet',
      },
      {
        id: 7777,
        username: 'abc1',
        name: 'The Rock',
        payment: 50,
        type: 'dinein',
        paymethod: 'wallet',
      },
      {
        id: 361,
        username: 'abc1',
        name: 'Bill Games',
        payment: 200,
        type: 'delivery',
        paymethod: 'cash',
      },
      {
        id: 9090,
        username: 'abc1',
        name: 'Mark Zinger',
        payment: 190,
        type: 'delivery',
        paymethod: 'wallet',
      },
    ];
    return (
      <div className="history">
        <div className="controls">
          <div className="search-container">
            <form action="#" onSubmit={this.search}>
              <Input type="text" placeholder="Search by #" />
              <Input type="text" placeholder="Search by Name" />
              <Input type="select">
                <option disabled>Select Type</option>
                <option>Any</option>
                <option>Delivery</option>
                <option>Dine In</option>
                <option>Takeaway</option>
              </Input>
              <Input type="select">
                <option disabled>Select Payment Method</option>
                <option>Any</option>
                <option>Cash</option>
                <option>Wallet</option>
              </Input>
              <Input type="date" placeholder="date" />
              <Button type="submit" className="search-btn" color="white"><img src={searchIcon} alt="search" /> Search</Button>
            </form>
          </div>
          <div className="info">
            <p>Showing results from last week</p>
            <Button color="white">Download .csv</Button>
          </div>
        </div>
        <HistoryTable
          onEntryIdClicked={this.showEntryModal}
          onNameClicked={this.showUserDetailsModal}
          data={data}
        />
        <Pagination>
          <PaginationItem>
            <PaginationLink previous href="#" />
          </PaginationItem>
          <PaginationItem>
            <PaginationLink next href="#" />
          </PaginationItem>
        </Pagination>
      </div>
    );
  }
}

History.propTypes = {
  showModal: PropTypes.func.isRequired,
};

export default connect(null, dispatch => ({
  showModal: (modalType, modalProps) => dispatch(showModal(modalType, modalProps)),
}))(History);
