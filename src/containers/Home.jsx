import React, { Component } from 'react';
import { withRouter, Switch, Route, NavLink } from 'react-router-dom';
import PropTypes from 'prop-types';
import Wrapper from '../components/Wrapper';
import '../styles/home.css';

const Delivery = Wrapper(() => import('./Delivery.jsx'));
const DineIn = Wrapper(() => import('./DineIn.jsx'));
const TakeAway = Wrapper(() => import('./TakeAway.jsx'));

class Home extends Component {
  constructor(props) {
    super(props);
    this.state = {
      deliveryCount: 10,
      dineInCount: 2,
      takeAwayCount: 3,
    };
  }

  componentDidMount() {
    if (this.props.history.location.pathname === '/') {
      this.props.history.replace('/delivery');
    }
  }

  isNavLinkDeliveryActive = (match, location) => {
    const d = '/delivery';
    return (
      location.pathname === `${d}/new` ||
      location.pathname === `${d}/inprogress` ||
      location.pathname === `${d}/track`
    );
  }

  isNavLinkDineInActive = (match, location) => {
    const d = '/dinein';
    return (
      location.pathname === `${d}/requests` ||
      location.pathname === `${d}/bookings`
    );
  }

  isNavLinkTakeAwayActive = (match, location) => {
    const d = '/takeaway';
    return (
      location.pathname === `${d}/new` ||
      location.pathname === `${d}/inprogress` ||
      location.pathname === `${d}/inwaiting`
    );
  }
  resetCounter = label => this.setState({ [label]: 0 });

  renderCounter = count => (
    count ? <span className="counter-bubble">{count}</span> : null
  )
  render() {
    const { deliveryCount, dineInCount, takeAwayCount } = this.state;
    return (
      <div className="home">
        <div className="head-links">
          <NavLink to="/delivery/new" isActive={this.isNavLinkDeliveryActive} onClick={() => this.resetCounter('deliveryCount')} className="custom-link link-delivery" activeClassName="active">Delivery / Gift {this.renderCounter(deliveryCount)}</NavLink>
          <NavLink to="/dinein/requests" isActive={this.isNavLinkDineInActive} onClick={() => this.resetCounter('dineInCount')} className="custom-link link-dinein" activeClassName="active">Dine In {this.renderCounter(dineInCount)}</NavLink>
          <NavLink to="/takeaway/new" isActive={this.isNavLinkTakeAwayActive} onClick={() => this.resetCounter('takeAwayCount')} className="custom-link link-takeaway" activeClassName="active">Takeaway {this.renderCounter(takeAwayCount)}</NavLink>
        </div>
        <Switch>
          <Route path="/delivery" component={Delivery} />
          <Route path="/dinein" component={DineIn} />
          <Route path="/takeaway" component={TakeAway} />
        </Switch>
      </div>
    );
  }
}

Home.propTypes = {
  history: PropTypes.object.isRequired,
};

export default withRouter(Home);
