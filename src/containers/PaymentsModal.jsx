import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Modal, ModalHeader, ModalBody, ModalFooter } from 'reactstrap';
import { connect } from 'react-redux';
import ReactPlaceholder from 'react-placeholder';
import { hideModal } from '../redux/actions/modal';

class EntryDetailsModal extends Component {
  state = { }
  render() {
    const { hide, entryId } = this.props;
    return (
      <Modal
        isOpen
        fade={false}
        toggle={hide}
      >
        <ModalHeader>Pay for payment#{entryId}</ModalHeader>
        <ModalBody>
          <ReactPlaceholder type="text" ready={false} rows={6} color="#eaeaea">Hello</ReactPlaceholder>
        </ModalBody>
        <ModalFooter>
          <button className="custom-btn" onClick={hide}>Cancel</button>
        </ModalFooter>
      </Modal>
    );
  }
}

EntryDetailsModal.propTypes = {
  hide: PropTypes.func.isRequired,
  entryId: PropTypes.number.isRequired,
};

export default connect(null, dispatch => ({
  hide: () => dispatch(hideModal()),
}))(EntryDetailsModal);
