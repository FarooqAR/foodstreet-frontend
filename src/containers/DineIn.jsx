import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Switch, Route, NavLink, withRouter } from 'react-router-dom';
import DineInCard from '../components/DineInCard';
import '../styles/cards.css';

class DineIn extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }
  componentDidMount() {
    if (this.props.history.location.pathname === '/dinein') {
      this.props.history.replace('/dinein/requests');
    }
  }
  renderCards = (cards, type) => cards.map(card => (
    <DineInCard
      id={card.id}
      key={card.id}
      userBy={card.userBy}
      time={card.time}
      timeIn={card.timeIn}
      heads={card.heads}
      preOrder={card.preOrder}
      booked={type === 'booked'}
    />
  ))
  render() {
    const cards = [
      {
        id: 0,
        userBy: { name: 'Farooq' },
        time: 1526896889664,
        heads: 4,
        preOrder: [
          {
            title: 'Karai',
            quantity: 1,
            price: 600,
            id: 0,
          },
          {
            title: 'Kabuli Palao',
            quantity: 1,
            price: 200,
            id: 1,
          },
        ],
        timeIn: 1526896889664 + (1000 * 60 * 60 * 5),
      },
      {
        id: 1,
        userBy: { name: 'Ahmed' },
        time: 1526896889664,
        heads: 2,
        timeIn: 1526896889664 + (1000 * 60 * 60 * 2),
      },
      {
        id: 2,
        userBy: { name: 'Bilal' },
        time: 1526896889664,
        heads: 6,
        timeIn: 1526896889664 + (1000 * 60 * 60 * 24),
      },
      {
        id: 3,
        userBy: { name: 'Khalid' },
        time: 1526896889664,
        heads: 7,
        timeIn: 1526896889664 + (1000 * 60 * 60 * 5),
      },
    ];
    return (
      <div className="dinein">
        <div className="controls">
          <NavLink to="/dinein/requests" className="custom-link" activeClassName="active">Requests</NavLink>
          <NavLink to="/dinein/bookings" className="custom-link">Bookings</NavLink>
        </div>
        <Switch>
          <Route
            exact
            path="/dinein/requests"
            component={() => (
              <div className="cards">
                {this.renderCards(cards)}
              </div>
            )}
          />
          <Route
            exact
            path="/dinein/bookings"
            component={() => (
              <div className="cards">
                {this.renderCards(cards.slice(2), 'booked')}
              </div>
            )}
          />
        </Switch>
      </div>
    );
  }
}

DineIn.propTypes = {
  history: PropTypes.object.isRequired,
};

export default withRouter(DineIn);
