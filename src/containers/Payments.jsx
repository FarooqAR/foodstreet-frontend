import React, { Component } from 'react';
import { Button, Input, Pagination, PaginationItem, PaginationLink } from 'reactstrap';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { showModal } from '../redux/actions/modal';
import { PAYMENTS_MODAL } from '../redux/actions/types';
import searchIcon from '../images/search16x16.png';
import PaymentsTable from '../components/PaymentsTable';
import { months } from '../values';
import '../styles/payments.css';

class Payments extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }
  showEntryModal = () => {
    // this.props.showModal(ENTRY_DETAILS_MODAL, {
    //   entryId,
    // });
  }
  showPaymentModal = (entryId) => {
    this.props.showModal(PAYMENTS_MODAL, {
      entryId,
    });
  }
  render() {
    const data = [
      {
        id: 0,
        from: '1/1/18 12:00am',
        to: '7/1/18 11:59pm',
        amount: 200,
        cutOff: 30,
        amountToBePaid: 30,
        dueDate: '14/1/18 11:59pm',
        status: 'pending',
        duePast: Date.now() > 1529521200000,
      },
      {
        id: 1,
        from: '1/1/18 12:00am',
        to: '7/1/18 11:59pm',
        amount: 200,
        cutOff: 30,
        amountToBePaid: 30,
        dueDate: '14/1/18 11:59pm',
        status: 'paid',
        paidOn: '12/June/18 11pm',
      },
      {
        id: 2,
        from: '1/1/18 12:00am',
        to: '7/1/18 11:59pm',
        amount: 200,
        cutOff: 30,
        amountToBePaid: 30,
        dueDate: '14/1/18 11:59pm',
        status: 'paid',
        paidOn: '12/June/18 11pm',
      },
      {
        id: 3,
        from: '1/1/18 12:00am',
        to: '7/1/18 11:59pm',
        amount: 200,
        cutOff: 30,
        amountToBePaid: 30,
        dueDate: '14/1/18 11:59pm',
        status: 'paid',
        paidOn: '12/June/18 11pm',
      },
      {
        id: 4,
        from: '1/1/18 12:00am',
        to: '7/1/18 11:59pm',
        amount: 200,
        cutOff: 30,
        amountToBePaid: 30,
        dueDate: '14/1/18 11:59pm',
        status: 'paid',
        paidOn: '12/June/18 11pm',
      },
      {
        id: 5,
        from: '1/1/18 12:00am',
        to: '7/1/18 11:59pm',
        amount: 200,
        cutOff: 30,
        amountToBePaid: 30,
        dueDate: '14/1/18 11:59pm',
        paidOn: '12/June/18 11pm',
        status: 'paid',
      },
    ];
    return (
      <div className="payments">
        <div className="controls">
          <div className="search-container">
            <form action="#" onSubmit={this.search}>
              <span className="label">From:</span>
              <Input type="select">
                <option disabled>Select Month</option>
                {months.map((m, i) => <option key={m} value={i}>{m}</option>)}
              </Input>
              <Input type="select">
                <option disabled>Select Year</option>
                <option value="2018">2018</option>
                <option value="2017">2017</option>
              </Input>
              <span className="label">To:</span>
              <Input type="select">
                <option disabled>Select Month</option>
                {months.map((m, i) => <option key={m} value={i}>{m}</option>)}
              </Input>
              <Input type="select">
                <option disabled>Select Year</option>
                <option value="2018">2018</option>
                <option value="2017">2017</option>
              </Input>
              <Button type="submit" className="search-btn" color="white"><img src={searchIcon} alt="search" /> Search</Button>
            </form>
          </div>
          <div className="info">
            <p>Showing results from last year</p>
            <Button color="white">Download .csv</Button>
          </div>
        </div>
        <PaymentsTable
          onPaymentIdClicked={this.showEntryModal}
          onPayClicked={this.showPaymentModal}
          data={data}
        />
        <Pagination>
          <PaginationItem>
            <PaginationLink previous href="#" />
          </PaginationItem>
          <PaginationItem>
            <PaginationLink next href="#" />
          </PaginationItem>
        </Pagination>
      </div>
    );
  }
}

Payments.propTypes = {
  showModal: PropTypes.func.isRequired,
};

export default connect(null, dispatch => ({
  showModal: (modalType, modalProps) => dispatch(showModal(modalType, modalProps)),
}))(Payments);
