import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Button, Input } from 'reactstrap';
import { connect } from 'react-redux';
import CircularProgress from '@material-ui/core/CircularProgress';
import { login, clearLoginStatus } from '../redux/actions/auth';
import '../styles/login.css';

class Login extends Component {
  state = { email: '', password: '' }
  onEmailChange = e => this.setState({ email: e.target.value })
  onPasswordChange = e => this.setState({ password: e.target.value })
  onLoginSubmit = (e) => {
    e.preventDefault();
    const { email, password } = this.state;
    this.props.clearLoginStatus();
    this.props.login(email, password);
  }
  render() {
    const { email, password } = this.state;
    const loader = this.props.loginStatus.type === 'loading' ? (
      <CircularProgress className="loader" size={20} />
    ) : null;
    return (
      <div className="login-container">
        <div className="login-inner">
          <h3>Login</h3>
          <form action="#" onSubmit={this.onLoginSubmit}>
            <Input type="email" onChange={this.onEmailChange} value={email} placeholder="Email" />
            <Input type="password" onChange={this.onPasswordChange} value={password} placeholder="Password" />
            <div className="login-btn-container">
              <Button type="submit">Login</Button>
            </div>
            <div className="login-status">
              {loader}
              <p className={`login-message ${this.props.loginStatus.type}`}>
                {this.props.loginStatus.message}
              </p>
            </div>
          </form>
        </div>
      </div>
    );
  }
}

Login.propTypes = {
  login: PropTypes.func.isRequired,
  clearLoginStatus: PropTypes.func.isRequired,
  loginStatus: PropTypes.object,
};

Login.defaultProps = {
  loginStatus: { type: '', message: '' },
};

function mStateToProps(state) {
  return {
    loginStatus: state.loginStatus,
  };
}

function mDispatchToProps(dispatch) {
  return {
    login: (email, password) => dispatch(login(email, password)),
    clearLoginStatus: () => dispatch(clearLoginStatus()),
  };
}

export default connect(mStateToProps, mDispatchToProps)(Login);
