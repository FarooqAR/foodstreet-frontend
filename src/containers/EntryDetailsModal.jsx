import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Modal, ModalHeader, ModalBody, ModalFooter } from 'reactstrap';
import { connect } from 'react-redux';
import ReactPlaceholder from 'react-placeholder';
import { hideModal } from '../redux/actions/modal';

class EntryDetailsModal extends Component {
  state = { entryDetails: null, isLoaded: false }
  componentDidMount() {
    this.timer = setTimeout(() => {
      this.setState({
        entryDetails: {
          id: this.props.entryId,
          type: 'delivery',
          title: 'Zinger Buger',
          user: { name: 'Dummy name' },
          payment: 300,
          paymethod: 'cash',
        },
        isLoaded: true,
      });
    }, 3000);
  }
  componentWillUnmount() {
    clearTimeout(this.timer);
  }
  render() {
    const { hide } = this.props;
    const { entryDetails, isLoaded } = this.state;
    return (
      <Modal
        isOpen
        fade={false}
        toggle={hide}
      >
        <ModalHeader>Entry Details</ModalHeader>
        <ModalBody>
          <ReactPlaceholder type="text" ready={isLoaded} rows={6} color="#eaeaea">
            {entryDetails ?
              <div>
                <div>id: {entryDetails.id}</div>
                <div>type: {entryDetails.type}</div>
                <div>user: {entryDetails.user.name}</div>
                <div>title: {entryDetails.title}</div>
                <div>payment: {entryDetails.payment}</div>
                <div>payment via: {entryDetails.paymethod}</div>
              </div> : <span>Loading</span>}
          </ReactPlaceholder>
        </ModalBody>
        <ModalFooter>
          <button className="custom-btn" onClick={hide}>Cancel</button>
        </ModalFooter>
      </Modal>
    );
  }
}

EntryDetailsModal.propTypes = {
  hide: PropTypes.func.isRequired,
  entryId: PropTypes.number.isRequired,
};

export default connect(null, dispatch => ({
  hide: () => dispatch(hideModal()),
}))(EntryDetailsModal);
