import React from 'react';
import { connect } from 'react-redux';
import '../styles/statusbar.css';

type Props = {
  offline: boolean,
}

const StatusBar = (props: Props) => (
  <div className={`status-bar ${props.offline ? '' : 'hide'}`}>
    <p>You are offline</p>
  </div>
);

function mStateToProps(state) {
  return {
    offline: state.offline,
  };
}
export default connect(mStateToProps)(StatusBar);
