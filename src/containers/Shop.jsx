import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import { ADD_SHOP_ITEM_MODAL } from '../redux/actions/types';
import { showModal } from '../redux/actions/modal';
import AsyncImage from '../components/AsyncImage';
import signalIcon from '../images/cellular.svg';
import batteryIcon from '../images/battery.svg';
import wifiIcon from '../images/wifi.svg';
import appBarIcon from '../images/appbar-menu.svg';
import food1 from '../images/dummy/food_1.jpg';
import food3 from '../images/dummy/food_3.jpg';
import food4 from '../images/dummy/food_4.jpg';
import food6 from '../images/dummy/food_6.jpg';
import '../styles/shop.css';

const initialItems = [
  {
    id: 0,
    title: 'Zinger Burger',
    subtitle: 'With the new bun',
    imgUrl: food1,
    size: 'full',
    price: 400,
    tax: 12,
    extras: [
      {
        title: 'Topping',
        price: 32,
      },
    ],
  },
  {
    id: 1,
    title: 'Karai',
    subtitle: 'Taste it or die!',
    imgUrl: food3,
    size: 'half',
    price: 630,
    tax: 4,
    extras: [
    ],
  },
  {
    id: 3,
    title: 'Beef Burger',
    subtitle: 'Unparalleled flesh!',
    imgUrl: food6,
    size: 'half',
    price: 420,
    tax: 15,
    extras: [
    ],
  },
  {
    id: 2,
    title: 'Shimlaa Mirch',
    subtitle: 'Couldn\'t be more spicy!',
    imgUrl: food4,
    size: 'full',
    price: 1200,
    tax: 20,
    extras: [
    ],
  },
];

class Shop extends Component {
  state = { items: initialItems, tabBarValue: 0 }
  shouldBeHalf = () => {
    const { items } = this.state;
    let lastHalfCount = 0;
    for (let i = items.length - 1; i >= 0; i -= 1) {
      if (items[i].size === 'full') {
        break;
      }
      lastHalfCount += 1;
    }
    return lastHalfCount % 2 === 1;
  }
  showAddItemModal = () => {
    this.props.showModal(ADD_SHOP_ITEM_MODAL);
  }
  handleChange = (event, tabBarValue) => {
    this.setState({ tabBarValue });
  };
  renderItems = items => (
    items.map(item => (
      <div key={item.id} className={`item ${item.size}`}>
        <div className="content">
          <div className="title">{item.title}</div>
          <div className="subtitle">{item.subtitle}</div>
        </div>
        <div className="price">
          <div className="price-label">Rs. {item.price}</div>
          <div className="price-tax">Tax: {item.tax}%</div>
        </div>
        <AsyncImage src={`${item.imgUrl}`} width="100%" />
      </div>
    ))
  );
  render() {
    return (
      <div className="shop">
        <h3>Customize your Shop</h3>
        <div className="mobile">
          <div className="mobile__header">
            <div className="left">
              <AsyncImage src={signalIcon} width={20} height={20} />{' '}
              <span>Zong</span>
            </div>
            <div className="middle">
              10:45 AM
            </div>
            <div className="right">
              <AsyncImage src={wifiIcon} width={20} height={20} />{' '}
              <span>20%</span>{' '}
              <AsyncImage src={batteryIcon} width={20} height={20} />
            </div>
          </div>
          <div className="mobile__appbar">
            <div className="mobile__appbar-inner">
              <span>My Shop</span>
              <AsyncImage src={appBarIcon} />
            </div>
            <Tabs
              className="tabbar"
              value={this.state.tabBarValue}
              onChange={this.handleChange}
              indicatorColor="secondary"
              textColor="secondary"
              scrollable
              scrollButtons="auto"
            >
              <Tab label="Eid Special" />
              <Tab label="Deals" />
              <Tab label="Burgers" />
              <Tab label="Pizzas" />
              <Tab label="Desi Food" />
              <Tab label="Drinks" />
            </Tabs>
          </div>
          <div className="mobile__body">
            {this.renderItems(this.state.items)}
            <div tabIndex="-1" role="button" onClick={this.showAddItemModal} className={`item add ${this.shouldBeHalf() ? 'half' : 'full'}`} />
          </div>
        </div>
      </div>
    );
  }
}

Shop.propTypes = {
  showModal: PropTypes.func.isRequired,
};

export default connect(null, dispatch => ({
  showModal: (modalType, modalProps) => dispatch(showModal(modalType, modalProps)),
}))(Shop);
