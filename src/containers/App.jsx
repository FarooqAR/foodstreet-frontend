import React, { Component } from 'react';
import firebase from 'firebase/app';
import PropTypes from 'prop-types';
import { Switch, Route, Redirect, withRouter } from 'react-router-dom';
import { connect } from 'react-redux';
import { logout, verifyToken } from '../redux/actions/auth';
import Main from './Main';
import ModalRoot from './ModalRoot';
import Wrapper from '../components/Wrapper';
import Header from '../components/Header';
import Footer from '../components/Footer';
import LoaderScreen from '../components/LoaderScreen';
import StatusBar from '../containers/StatusBar';
import '../styles/app.css';

const Login = Wrapper(() => import('./Login'));

class App extends Component {
  static getDerivedStateFromProps(nextProps) {
    if (nextProps.verifyStatus) {
      return { loading: false, loggedIn: true };
    }
    return { loading: false, loggedIn: false };
  }
  constructor(props) {
    super(props);
    this.state = { loading: true, loggedIn: false };
    firebase.auth().onAuthStateChanged((user) => {
      if (user) {
        this.props.verifyToken(user);
      } else {
        this.setState({ loading: false, loggedIn: false });
      }
    });
  }
  render() {
    return (
      <div className="app">
        {
          this.props.history.location.pathname === '/login' &&
          this.state.loggedIn &&
          !this.state.loading ?
            <Redirect to="/delivery/new" /> :
          null}
        {this.state.loading ?
          <LoaderScreen /> :
          <Switch>
            <Route path="/login" component={Login} />
            <Route
              path="/"
              render={() => {
                if (this.props.history.location.pathname !== '/login' &&
                !this.state.loggedIn) {
                  return <Redirect to="/login" />;
                }
                return [
                  <Header onLogout={this.props.logout} key="header" />,
                  <StatusBar key="status" />,
                  <Main key="main" />,
                  <Footer key="footer" />,
                  <ModalRoot key="modalRoot" />,
                ];
              }}
            />
          </Switch>
        }
      </div>
    );
  }
}

App.propTypes = {
  logout: PropTypes.func.isRequired,
  verifyToken: PropTypes.func.isRequired,
  history: PropTypes.object.isRequired,
  verifyStatus: PropTypes.bool.isRequired,
};

function mDispatchToProps(dispatch) {
  return {
    logout: () => dispatch(logout()),
    verifyToken: currentUser => dispatch(verifyToken(currentUser)),
  };
}

function mStateToProps(state) {
  return {
    verifyStatus: state.verifyStatus,
  };
}

export default withRouter(connect(mStateToProps, mDispatchToProps)(App));
