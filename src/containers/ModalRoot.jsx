import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import Wrapper from '../components/Wrapper';
import { MAP_MODAL, ENTRY_DETAILS_MODAL, USER_DETAILS_MODAL, ADD_SHOP_ITEM_MODAL, PAYMENTS_MODAL } from '../redux/actions/types';
import '../styles/modal.css';

const MODAL_COMPONENTS = {
  [MAP_MODAL]: Wrapper(() => import('../components/MapModal')),
  [ENTRY_DETAILS_MODAL]: Wrapper(() => import('./EntryDetailsModal')),
  [USER_DETAILS_MODAL]: Wrapper(() => import('./UserDetailsModal')),
  [ADD_SHOP_ITEM_MODAL]: Wrapper(() => import('./AddShopItemModal')),
  [PAYMENTS_MODAL]: Wrapper(() => import('./PaymentsModal')),
};

const ModalRoot = ({ modalType, modalProps }) => {
  if (!modalType) {
    return null;
  }
  const Modal = MODAL_COMPONENTS[modalType];
  return <Modal {...modalProps} />;
};

ModalRoot.propTypes = {
  modalType: PropTypes.string,
  modalProps: PropTypes.object,
};

ModalRoot.defaultProps = {
  modalType: null,
  modalProps: {},
};

const mapStateToProps = state => state.modal;

export default connect(mapStateToProps)(ModalRoot);
