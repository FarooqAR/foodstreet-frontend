import React, { Component } from 'react';
import { Switch, Route } from 'react-router-dom';
import Wrapper from '../components/Wrapper';
import '../styles/main.css';

const Home = Wrapper(() => import('./Home'));
const History = Wrapper(() => import('./History'));
const Shop = Wrapper(() => import('./Shop'));
const Settings = Wrapper(() => import('./Settings'));
const Payments = Wrapper(() => import('./Payments'));

class Main extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }
  render() {
    return (
      <div className="main">
        <Switch>
          <Route path="/settings" component={Settings} />
          <Route path="/payments" component={Payments} />
          <Route path="/myshop" component={Shop} />
          <Route path="/history" component={History} />
          <Route path="/" component={Home} />
        </Switch>
      </div>
    );
  }
}

export default Main;
