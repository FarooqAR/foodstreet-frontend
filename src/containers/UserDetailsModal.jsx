import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Modal, ModalHeader, ModalBody, ModalFooter } from 'reactstrap';
import { connect } from 'react-redux';
import ReactPlaceholder from 'react-placeholder';
import { TextBlock, RoundShape } from 'react-placeholder/lib/placeholders';
import { hideModal } from '../redux/actions/modal';
import '../styles/userdetailsmodal.css';

class UserDetailsModal extends Component {
  state = { userDetails: null, isLoaded: false }
  componentDidMount() {
    console.log('mount');
  }
  componentWillUnmount() {
    console.log('unmount');
  }
  renderCustomPlaceholder = () => (
    <div className="user-details-modal-placeholder">
      <RoundShape color="#E0E0E0" style={{ width: 70, height: 70 }} />
      <TextBlock rows={4} color="#eaeaea" />
    </div>
  );
  render() {
    const { hide } = this.props;
    const { userDetails, isLoaded } = this.state;
    return (
      <Modal
        isOpen
        fade={false}
        toggle={hide}
      >
        <ModalHeader>User Details</ModalHeader>
        <ModalBody>
          <ReactPlaceholder customPlaceholder={this.renderCustomPlaceholder()} type="text" ready={isLoaded} rows={6} color="#eaeaea">
            {userDetails ?
              <div>
                <div>username: {userDetails.id}</div>
                <div>type: {userDetails.type}</div>
                <div>user: {userDetails.user.name}</div>
                <div>title: {userDetails.title}</div>
                <div>payment: {userDetails.payment}</div>
                <div>payment via: {userDetails.paymethod}</div>
              </div> : <span>Loading</span>}
          </ReactPlaceholder>
        </ModalBody>
        <ModalFooter>
          <button className="custom-btn" onClick={hide}>Cancel</button>
        </ModalFooter>
      </Modal>
    );
  }
}

UserDetailsModal.propTypes = {
  hide: PropTypes.func.isRequired,
};

export default connect(null, dispatch => ({
  hide: () => dispatch(hideModal()),
}))(UserDetailsModal);
